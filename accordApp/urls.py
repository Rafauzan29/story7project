from django.urls import path
from . import views

urlpatterns = [
	path('', views.askingMe, name = 'home'),
	path('collections', views.collections, name = 'coll'),
]