from django.apps import AppConfig


class AccordappConfig(AppConfig):
    name = 'accordApp'
