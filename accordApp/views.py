from django.shortcuts import render, redirect
from .forms import askingMeForm
from .models import Question_and_Answer


# Create your views here.

def askingMe(request):
	form = askingMeForm()

	if request.method == "POST":
		form = askingMeForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect('coll')

	context = {'form':form}
	return render(request, 'home.html', context)

def collections(request):
	QnA = Question_and_Answer.objects.all()
	context = {'QnA' : QnA}
	return render(request, 'collections.html', context)